{{- define "appsecauditor.secretsData" -}}
DB_PASS: {{ .Values.postgresql.auth.password | b64enc | quote }}
REDIS_PASSWORD: {{ .Values.redis.auth.password | b64enc | quote }}
{{- end -}}

{{- define "appsecauditor.secret.amqp" -}}
AMQP_HOST_STRING: {{ printf "amqp://%s:%s@%s:%s/" .Values.rabbitmq.auth.username .Values.rabbitmq.auth.password (include "rabbitmq.host" . ) .Values.rabbitmq.containerPorts.amqp | b64enc | quote }}
{{- end -}}"

{{- define "rabbitmq.host"}}
{{- printf "%s-%s" .Release.Name .Values.rabbitmq.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end }}    

{{- define "postgres.host"}}
{{- if .Values.configs.configMap.database.host }}
{{- .Values.configs.configMap.database.host | quote }}
{{- else }}
{{- printf "%s-%s" .Release.Name .Values.postgresql.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end }}
{{- end }}

{{- define "redis.host"}}
{{- if .Values.configs.configMap.redis.host }}
{{ .Values.configs.configMap.redis.host | quote }}
{{- else }}
{{- printf "%s-%s" .Release.Name .Values.redis.name | trunc 63 | trimSuffix "-" -}}
{{- end }}
{{- end }}

{{/*
Create auditor name
*/}}
{{- define "auditor.fullname" -}}
{{- printf "%s-%s" (include "appsecauditor.fullname" .) .Values.auditor.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create scanner-worker name
*/}}
{{- define "scannerWorker.fullname" -}}
{{- printf "%s-%s" (include "appsecauditor.fullname" .) .Values.scannerWorker.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}


{{/*
Create redis name
*/}}
{{- define "redis.fullname" -}}
{{- printf "%s-%s" (include "appsecauditor.fullname" .) .Values.redis.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
